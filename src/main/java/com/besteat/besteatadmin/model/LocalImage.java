package com.besteat.besteatadmin.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
public class LocalImage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String imagePath;

    @ManyToOne
    private Local local;

    public LocalImage(String imagePath, Local local) {
        this.imagePath = imagePath;
        this.local = local;
    }
}
