package com.besteat.besteatadmin.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Time;
import java.util.Set;

@NoArgsConstructor
@Data
@Entity
public class Local {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String address;
    private String city;
    private String description;
    private String geo_lat;
    private String geo_lng;
    private String phoneNumber;

    private Boolean status;

    private Boolean delivery_status;
    private float delivery_price;

    private Time open_time;
    private Time close_time;

    @ManyToOne
    private Restaurant restaurant;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "local")
    private Set<LocalImage> localImages;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "local")
    private Set<Offer> offers;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "local")
    private Set<Review> reviews;


    public Local(String address, String city, String description, String geo_lat, String geo_lng, String phoneNumber, Boolean status, Boolean delivery_status, float delivery_price, Time open_time, Time close_time, Restaurant restaurant) {
        this.address = address;
        this.city = city;
        this.description = description;
        this.geo_lat = geo_lat;
        this.geo_lng = geo_lng;
        this.phoneNumber = phoneNumber;
        this.status = status;
        this.delivery_status = delivery_status;
        this.delivery_price = delivery_price;
        this.open_time = open_time;
        this.close_time = close_time;
        this.restaurant = restaurant;
    }

}
