package com.besteat.besteatadmin.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@Data
@Entity
public class MealType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String imagePath;

    @ManyToOne
    private Cuisine cuisine;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mealType")
    private Set<Meal> meals;

    public MealType(String name, String imagePath, Cuisine cuisine) {
        this.name = name;
        this.imagePath = imagePath;
        this.cuisine = cuisine;
    }
}
