package com.besteat.besteatadmin.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private float rate;
    private String title;
    private String comment;

    @ManyToOne
    private Local local;

    @ManyToOne
    private User user;

    public Review(float rate, String title, String comment, Local local, User user) {
        this.rate = rate;
        this.title = title;
        this.comment = comment;
        this.local = local;
        this.user = user;
    }
}
