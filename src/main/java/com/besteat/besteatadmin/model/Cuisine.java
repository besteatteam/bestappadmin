package com.besteat.besteatadmin.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@Data
@Entity
public class Cuisine {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String imagePath;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cuisine")
    private Set<MealType> mealTypes;

    public Cuisine(String name, String imagePath) {
        this.name = name;
        this.imagePath = imagePath;
    }
}
