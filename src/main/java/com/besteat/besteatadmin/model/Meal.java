package com.besteat.besteatadmin.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@Data
@Entity
public class Meal {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String description;
    private float price;
    private String imagePath;

    @ManyToOne
    private MealType mealType;

    @ManyToOne
    private Restaurant restaurant;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "meal")
    private Set<BookmarkMeal> bookmarkMeals;

    public Meal(String name, String description, float price, String imagePath, MealType mealType, Restaurant restaurant) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.imagePath = imagePath;
        this.mealType = mealType;
        this.restaurant = restaurant;
    }
}
