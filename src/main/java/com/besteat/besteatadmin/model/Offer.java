package com.besteat.besteatadmin.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Data
@Entity
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;
    private String description;
    private Date deadline;

    @ManyToOne
    private Local local;

    public Offer(String title, String description, Date deadline, Local local) {
        this.title = title;
        this.description = description;
        this.deadline = deadline;
        this.local = local;
    }
}
