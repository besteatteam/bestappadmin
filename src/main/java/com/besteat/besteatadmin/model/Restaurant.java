package com.besteat.besteatadmin.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@Data
@Entity
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String description;
    private String logoPath;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "restaurant")
    private Set<Local> locals;

    public Restaurant(String name, String description, String logoPath) {
        this.name = name;
        this.description = description;
        this.logoPath = logoPath;
    }
}
