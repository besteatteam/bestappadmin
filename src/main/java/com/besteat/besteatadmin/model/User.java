package com.besteat.besteatadmin.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Set;


@NoArgsConstructor
@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @Email
    private String email;

    private String phoneNumber;

    @Length(min=5)
    private String password;

    private String photoPath;

    private boolean enabled;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<Review> reviews;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<BookmarkMeal> bookmarkMeals;

    public User(String name, @Email String email, String phoneNumber, @Length(min = 5) String password, String photoPath, boolean enabled) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.photoPath = photoPath;
        this.enabled = enabled;
    }
}
