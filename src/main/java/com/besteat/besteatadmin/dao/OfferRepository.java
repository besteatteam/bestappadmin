package com.besteat.besteatadmin.dao;

import com.besteat.besteatadmin.model.Offer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfferRepository extends JpaRepository<Offer, Long> {

}
