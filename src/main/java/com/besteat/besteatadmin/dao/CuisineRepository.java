package com.besteat.besteatadmin.dao;

import com.besteat.besteatadmin.model.Cuisine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CuisineRepository extends JpaRepository<Cuisine, Long> {

}
