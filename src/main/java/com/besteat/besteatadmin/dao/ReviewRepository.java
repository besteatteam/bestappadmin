package com.besteat.besteatadmin.dao;

import com.besteat.besteatadmin.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepository extends JpaRepository<Review, Long> {

}
