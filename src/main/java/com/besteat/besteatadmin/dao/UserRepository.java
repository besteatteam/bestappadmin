package com.besteat.besteatadmin.dao;

import com.besteat.besteatadmin.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
