package com.besteat.besteatadmin.dao;

import com.besteat.besteatadmin.model.LocalImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocalImageRepository extends JpaRepository<LocalImage, Long> {

}
