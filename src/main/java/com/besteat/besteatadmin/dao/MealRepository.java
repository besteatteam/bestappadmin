package com.besteat.besteatadmin.dao;

import com.besteat.besteatadmin.model.Meal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MealRepository extends JpaRepository<Meal, Long> {

}
