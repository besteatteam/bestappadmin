package com.besteat.besteatadmin.dao;

import com.besteat.besteatadmin.model.BookmarkMeal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookmarkMealRepository extends JpaRepository<BookmarkMeal, Long> {

}
