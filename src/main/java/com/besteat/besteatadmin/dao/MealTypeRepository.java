package com.besteat.besteatadmin.dao;

import com.besteat.besteatadmin.model.MealType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MealTypeRepository extends JpaRepository<MealType, Long> {

}
