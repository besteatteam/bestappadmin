package com.besteat.besteatadmin.dao;

import com.besteat.besteatadmin.model.Local;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocalRepository extends JpaRepository<Local, Long> {

}
