package com.besteat.besteatadmin.dao;

import com.besteat.besteatadmin.model.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {

}
