package com.besteat.besteatadmin.service;

import com.besteat.besteatadmin.model.Local;
import com.besteat.besteatadmin.model.Restaurant;
import com.besteat.besteatadmin.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomeService {

    @Autowired private BookmarkMealRepository bookmarkMealRepository;
    @Autowired private CuisineRepository cuisineRepository;
    @Autowired private LocalImageRepository localImageRepository;
    @Autowired private LocalRepository localRepository;
    @Autowired private MealRepository mealRepository;
    @Autowired private MealTypeRepository mealTypeRepository;
    @Autowired private OfferRepository offerRepository;
    @Autowired private RestaurantRepository restaurantRepository;
    @Autowired private ReviewRepository reviewRepository;
    @Autowired private UserRepository userRepository;



    public List<Restaurant> getRestaurants(){
        return restaurantRepository.findAll();
    }

    public void saveRestaurant(Restaurant restaurant){
        restaurantRepository.save(restaurant);
    }

    public List<Local> getLocals(){
        return localRepository.findAll();
    }

    public void saveLocal(Local local) {
        localRepository.save(local);
    }
}
