package com.besteat.besteatadmin;

import com.besteat.besteatadmin.model.Local;
import com.besteat.besteatadmin.model.Restaurant;
import com.besteat.besteatadmin.dao.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BestAppApiApplication {

    private Logger log = LoggerFactory.getLogger(BestAppApiApplication.class);


    @Autowired
    private BookmarkMealRepository bookmarkMealRepository;
    @Autowired
    private CuisineRepository cuisineRepository;
    @Autowired
    private LocalImageRepository localImageRepository;
    @Autowired
    private LocalRepository localRepository;
    @Autowired
    private MealRepository mealRepository;
    @Autowired
    private MealTypeRepository mealTypeRepository;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private RestaurantRepository restaurantRepository;
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private UserRepository userRepository;

    @Bean
    public void init() {

        restaurantRepository.save(new Restaurant("first", "pooow", "opop"));
        restaurantRepository.save(new Restaurant("q", "ew", "d"));
        restaurantRepository.save(new Restaurant("q", "ew", "d"));

        localRepository.save(new Local("chbanate", "Rabat", "blaaa", "11111", "222222",
                "0606060606", true, true, 7f, null, null, null));

    }

    public static void main(String[] args) {
        SpringApplication.run(BestAppApiApplication.class, args);
    }

}

