package com.besteat.besteatadmin.controller;

import com.besteat.besteatadmin.service.HomeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class HomeController {

    private Logger log = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private HomeService homeService;

    @RequestMapping("/")
    public String index(){
        return "index";
    }

    @RequestMapping("/restaurants")
    public String ShowRestaurants(){
        return "restaurants";
    }


//    @RequestMapping(value = "/restaurants", method = RequestMethod.GET)
//    public List<Restaurant> getRestaurants(){
//        return homeService.getRestaurants();
//    }
//
//    @RequestMapping(value = "/restaurants", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public void setRestaurants(@RequestBody Restaurant restaurant){
//        homeService.saveRestaurant(restaurant);
//    }
//
//    @RequestMapping("/locals")
//    public List<Local> getLocals(){
//        return homeService.getLocals();
//    }
//
//    @RequestMapping(value = "/locals", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public void setLocals(@RequestBody Local local){
//        homeService.saveLocal(local);
//    }

}
